const ENV = {
  ipushpull: {
    api_url: "https://test.ipushpull.com/api",
    ws_url: "https://test.ipushpull.com",
    web_url: "https://test.ipushpull.com",
    docs_url: "https://test.ipushpull.com/docs",
    billing_url: "https://test.ipushpull.com/billing",
    help_url: "http://support.ipushpull.com",
    auth_url: "https://test.ipushpull.com/auth",
    app_url: "https://test.ipushpull.com/pages",
    embed_url: "https://test.ipushpull.com/embed",
    chat_url:
      "https://test.ipushpull.com/embedded/integrations/symphony/chat.html",
    api_key: "LC8aOmGqqydL3n8HyKaz8ue34M13WPux2zyuBAl1",
    api_secret:
      "kXXRR7jsoEm6JgjvBDYGca1YlGIDynuxpM9kiu9GTlqzLtmCvNWLm3pmzWUc6Uptp4DwvS1kLlC6mh5PHGGnKRNh0eBPG1IIUB5NtsEY1oSgqLcteNCdGBLnIwnOO7SY",
    hsts: false,
    storage_prefix: "ipp_test",
    transport: "",
    client_version: "test-1.0.0",
  },
  clients: {
    ipushpull: {
      app: {
        auth: {
          ipushpull: {
            client_version: "auth-1.0.0",
          },
          images: [
            "/auth/backgrounds/01.jpg",
            "/auth/backgrounds/02.jpg",
            "/auth/backgrounds/03.jpg",
          ],
        },
        embed: {
          ipushpull: {
            client_version: "embed-test",
          },
          plugins: false,
        },
        client: {
          ipushpull: {
            client_version: "client-test",
          },
          stripe: {
            key: "",
          },
          states_menu: true,
          pinned_folders: true,
          formula_bar: true,
          plugins: false,
          page_schema_builder: false,
          page_query: false,
          notifications: false,
          lsd: false,
          row_selection: true,
          addin_url: "https://ipushpull.s3.eu-west-1.amazonaws.com/add-in/iPushPullExcelSetup20.11.1.0.msi",
          theme: {
            ui: {
              folder_management_usage: false,
            },
          },
          lsd_apps: [
            {
              id: 2,
              name: "Desktop app",
              key: "ipp",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
            },
            {
              id: 3,
              name: "Excel Add-in",
              key: "excel",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/excel.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
            },
            {
              id: 38,
              name: "Microsoft Teams",
              key: "teams",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/teams.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
            },
            {
              id: 14,
              name: "Symphony",
              key: "symphony",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/symphony.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
            },
            {
              id: -1,
              name: "WordPress",
              key: "wp",
              logo: {
                light:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/wp.svg",
                dark:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/wp-white.svg",
              },
              help_doc: "https://ipushpull.atlassian.net/wiki/x/a4CBKQ",
            },
          ],
        },
      },
    },
  },
  sso_only: false,
  sso_url: "https://test.ipushpull.com/api/1.0/sso/TQ6M5Qe9GJXqm6coP4pBD6/",
  on_prem: true,
  billing: false,
  help_docs: {
    livestreaming_data_source_excel: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AYCcJg",
      iframe: "https://test.ipushpull.com/help.php?id=647790593",
    },
    livestreaming_data_source_db: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AYDfC",
      iframe: "https://test.ipushpull.com/help.php?id=148865025",
    },
    livestreaming_data_source_api: {
      desk: "https://ipushpull.atlassian.net/wiki/x/CYCgK",
      iframe: "https://test.ipushpull.com/help.php?id=681607177",
    },
    livestreaming_client_app_ipp: {
      desk: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
      iframe: "https://test.ipushpull.com/help.php?id=681705505",
    },
    livestreaming_client_app_excel: {
      desk: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
      iframe: "https://test.ipushpull.com/help.php?id=156074055",
    },
    livestreaming_client_app_symphony: {
      desk: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
      iframe: "https://test.ipushpull.com/help.php?id=695959587",
    },
    livestreaming_client_app_wp: {
      desk: "https://ipushpull.atlassian.net/wiki/x/a4CBKQ",
      iframe: "https://test.ipushpull.com/help.php?id=696352875",
    },
    livestreaming_client_app_teams: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
      iframe: "https://test.ipushpull.com/help.php?id=678494209",
    },
    notifications_builder_excel_data_source: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AYDmI",
      iframe: "https://test.ipushpull.com/help.php?id=551976961",
    },
    notifications_builder_msteams_webhook: {
      desk: "https://ipushpull.atlassian.net/wiki/x/H4BeHg",
      iframe: "https://test.ipushpull.com/help.php?id=509509663",
    },
    notifications_builder_slack_webhook: {
      desk: "https://ipushpull.atlassian.net/wiki/x/JgBfHg",
      iframe: "https://test.ipushpull.com/help.php?id=509542438",
    },
    notifications_builder_symphony_webhook: {
      desk: "https://ipushpull.atlassian.net/wiki/x/LwBfHg",
      iframe: "https://test.ipushpull.com/help.php?id=509542447",
    },
    symphony_overview: {
      desk: "https://ipushpull.atlassian.net/wiki/x/UYCHDQ",
      iframe: "https://test.ipushpull.com/help.php?id=226984017",
    },
  },
};
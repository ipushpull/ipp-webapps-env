const ENV = {
  ipushpull: {
    api_url: "http://127.0.0.1:8000/api",
    app_url: "http://127.0.0.1:8081",
    auth_url: "http://127.0.0.1:8999",
    help_url: "http://support.ipushpull.com",
    api_key: "LC8aOmGqqydL3n8HyKaz8ue34M13WPux2zyuBAl1",
    api_secret:
      "kXXRR7jsoEm6JgjvBDYGca1YlGIDynuxpM9kiu9GTlqzLtmCvNWLm3pmzWUc6Uptp4DwvS1kLlC6mh5PHGGnKRNh0eBPG1IIUB5NtsEY1oSgqLcteNCdGBLnIwnOO7SY",
    hsts: false,
    storage_prefix: "ipp_local",
    transport: "polling",
  },
  clients: {
    ipushpull: {
      app: {
        auth: {
          ipushpull: {
            client_version: "auth-1.0.0",
          },
          images: [
            "/backgrounds/01.jpg",
            "/backgrounds/02.jpg",
            "/backgrounds/03.jpg",
          ],
        },
        client: {
          ipushpull: {
            client_version: "client-local",
          },
          stripe: {
            key: "pk_test_fK2oqzZxVdpohta5kABg4j9P",
          },
          disclaimer: [
            "COEX Partners is a division of Tullett Prebon Financial Services LLC (â€œTullett Prebonâ€), an introducing broker registered with the U.S. Commodity Futures Trading Commission and a broker-dealer registered with the U.S. Securities and Exchange Commission.  This communication has been prepared for information purposes only.  It does not constitute a recommendation or solicitation to buy, sell, subscribe for or underwrite any financial instrument or product. Its information is confidential and may not be disclosed or reproduced in whole or part under any circumstances without the prior written consent of Tullett Prebon. The information or opinions contained in this communication are obtained or derived from sources generally believed to be reliable and in good faith, but Tullett Prebon makes no representations as to their accuracy or completeness. Opinions and views expressed are subject to change without notice, as are prices and availability, which are indicative only.  There is no obligation to notify recipients of any changes to this information or to do so in the future. No responsibility or liability is accepted for the use of or reliance on the information, strategies, tools, or services presented in this document.",
            "This communication is not directed to, or intended for distribution to or use by, any person or entity who is a citizen or resident of or located in any locality, state, country or other jurisdiction where such distribution, publication, availability or use would be contrary to law or regulation or which would subject Tullett Prebon or its affiliates to any registration or licensing requirement within such jurisdiction.",
            "Tullett Prebon Financial Services LLC is a member of FINRA, NFA and the Securities Investor Protection Corp",
          ],
          states_menu: true,
          pinned_folders: true,
          formula_bar: true,
          plugins: true,
          page_schema_builder: false,
          page_query: true,
          notifications: true,
          lsd: true,
          dod: true,
          row_selection: true,
          release_notes: "https://support.ipushpull.com",
          experimental_features: true,
          theme: {
            upgrade: {
              version: "2021.3.3",
              type: "major",
              release: {
                label: "Find out more",
                help_doc: "https://support.ipushpull.com",
              },
              message:
                "<p>There's a new version of the ipushpull app available, which includes:</p><ul><li>Streamlined onboarding for new and invited users</li><li>Microsoft Teams and Slack integrations coming soon - get in touch for a preview</li></ul>",
              logo: {
                light:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/static/logo-new.svg",
                dark:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/static/logo-white.svg",
              },
            },
          },
          services: [
            {
              name: "Desktop",
              key: "ipp",
              serviceType: ["lsd"],
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
            },
            {
              name: "Mobile",
              key: "mobile",
              serviceType: ["lsd"],
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
            },
            {
              name: "Excel Add-in",
              key: "excel",
              serviceType: ["lsd"],
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/excel.png",
            },
            {
              name: "Microsoft Teams",
              key: "teams",
              botKey: "teamsBot",
              serviceType: ["lsd", "dod", "ddn"],
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/teams.png",
            },
            {
              name: "Symphony",
              key: "symphony",
              botKey: "symphonyBot",
              serviceType: ["lsd", "dod", "ddn"],
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/symphony.png",
            },
            {
              name: "Slack",
              key: "slack",
              serviceType: ["dod", "ddn"],
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/slack.png",
            },
            {
              name: "WhatsApp",
              key: "whatsApp",
              serviceType: ["ddn"],
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/whatsapp.png",
            },
            {
              name: "SMS",
              key: "sms",
              serviceType: ["ddn"],
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/sms.png",
            },
            {
              name: "Email",
              key: "email",
              serviceType: ["ddn"],
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/email.png",
            },
          ],
          data_sources: [
            {
              id: 3,
              name: "Excel Add-in",
              key: "excel",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/excel.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
              appType: ["lsd"],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/AYCcJg",
                  iframe: "https://test.ipushpull.com/help.php?id=647790593",
                },
                dod: {
                  desk: "",
                  iframe: "",
                },
              },
            },
            {
              id: -1,
              name: "Database",
              key: "db",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/db.svg",
              // help_doc: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
              appType: ["lsd"],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/AYDfC",
                  iframe: "https://test.ipushpull.com/help.php?id=148865025",
                },
                dod: {
                  desk: "",
                  iframe: "",
                },
              },
            },
            {
              id: -1,
              name: "API",
              key: "api",
              logo: "",
              appType: ["lsd"],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/l/c/xsgmc85x",
                  iframe: "https://test.ipushpull.com/help.php?id=681607177",
                },
                dod: {
                  desk: "",
                  iframe: "",
                },
              },
            },
          ],
          client_apps: [
            {
              id: 2,
              name: "Desktop App",
              key: "ipp",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
                  iframe: "https://test.ipushpull.com/help.php?id=681705505",
                },
                dod: {
                  desk: "",
                  iframe: "",
                },
              },
              appType: ["lsd"],
            },
            {
              id: 41,
              name: "Mobile App",
              key: "mobile",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
              appType: ["lsd"],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/l/c/qxv9HZC1",
                  iframe: "https://test.ipushpull.com/help.php?id=650248196",
                },
                dod: {
                  desk: "",
                  iframe: "",
                },
              },
            },
            {
              id: 3,
              name: "Excel Add-in",
              key: "excel",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/excel.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
              appType: ["lsd"],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
                  iframe: "https://test.ipushpull.com/help.php?id=156074055",
                },
                dod: {
                  desk: "",
                  iframe: "",
                },
              },
            },
            {
              id: 38,
              name: "Microsoft Teams App",
              key: "teams",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/teams.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
              appType: ["lsd"],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
                  iframe: "https://test.ipushpull.com/help.php?id=678494209",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/aut0u3UB",
                  iframe: "https://test.ipushpull.com/help.php?id=741670913",
                },
              },
            },
            {
              id: 42,
              name: "Microsoft Teams Bot",
              key: "teamsBot",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/teams.png",
              appType: ["dod"],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
                  iframe: "https://test.ipushpull.com/help.php?id=678494209",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/aut0u3UB",
                  iframe: "https://test.ipushpull.com/help.php?id=741670913",
                },
              },
            },
            {
              id: 7,
              name: "Slack Bot",
              key: "slack",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/slack.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
              appType: ["dod"],
              help: {
                lsd: {
                  desk: "",
                  iframe: "",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/NcSUB3rn",
                  iframe: "https://test.ipushpull.com/help.php?id=1442807809",
                },
              },
            },
            {
              id: 14,
              name: "Symphony App",
              key: "symphony",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/symphony.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
              appType: ["lsd"],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
                  iframe: "https://test.ipushpull.com/help.php?id=695959587",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/DeKPs5es",
                  iframe: "https://test.ipushpull.com/help.php?id=742162433",
                },
              },
            },
            {
              id: 43,
              name: "Symphony Bot",
              key: "symphonyBot",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/symphony.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
              appType: ["dod"],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
                  iframe: "https://test.ipushpull.com/help.php?id=695959587",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/DeKPs5es",
                  iframe: "https://test.ipushpull.com/help.php?id=742162433",
                },
              },
            },

            // {
            //   id: -1,
            //   name: "WordPress",
            //   key: "wp",
            //   logo: {
            //     light:
            //       "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/wp.svg",
            //     dark:
            //       "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/wp-white.svg",
            //   },
            //   help_doc: "https://ipushpull.atlassian.net/wiki/x/a4CBKQ",
            // },
          ],
          lsd_apps: [
            {
              id: 2,
              name: "Desktop app",
              key: "ipp",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
              help: [
                {
                  title: "Client App overview",
                  url: "https://ipushpull.atlassian.net/l/c/PHy2bZn0",
                },
                {
                  title: "Pull Live & Streaming Data",
                  url: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
                },
                {
                  title: "Find out more ...",
                  url: "https://ipushpull.atlassian.net/l/c/7j4QXJHj",
                },
              ],
            },
            {
              id: 41,
              name: "Mobile app",
              key: "mobile",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
            },
            {
              id: 3,
              name: "Excel Add-in",
              key: "excel",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/excel.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
              help: [
                {
                  title: "Install the Excel Add-in",
                  url: "https://ipushpull.atlassian.net/l/c/NXJ1Js3d",
                },
                {
                  title: "Pull Live & Streaming Data",
                  url: "https://ipushpull.atlassian.net/l/c/WHeWjzmk",
                },
                {
                  title: "Find out more ...",
                  url: "https://ipushpull.atlassian.net/l/c/L9oPttf9",
                },
              ],
            },
            {
              id: 38,
              name: "Microsoft Teams",
              key: "teams",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/teams.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
              help: [
                {
                  title: "Install the Teams app",
                  url: "https://ipushpull.atlassian.net/l/c/TuecR7Ar",
                },
                {
                  title: "Pull Live & Streaming Data",
                  url: "https://ipushpull.atlassian.net/l/c/U0CeB7fZ",
                },
                {
                  title: "Find out more ...",
                  url: "https://ipushpull.atlassian.net/l/c/sZorb7Kt",
                },
              ],
            },
            {
              id: 14,
              name: "Symphony",
              key: "symphony",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/symphony.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
              help: [
                {
                  title: "Symphony app overview",
                  url: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
                },
                {
                  title: "Pull Live & Streaming Data",
                  url: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
                },
                {
                  title: "Find out more ...",
                  url: "https://ipushpull.atlassian.net/l/c/VtU22Dzj",
                },
              ],
            },
          ],
        }
      },
    },
  },
  on_prem: false,
  billing: true,
  eu_vat_countries: [
    "AT",
    "BE",
    "BG",
    "HR",
    "CY",
    "CZ",
    "DK",
    "EE",
    "FI",
    "FR",
    "DE",
    "EL",
    "HU",
    "IE",
    "IT",
    "LV",
    "LT",
    "LU",
    "MT",
    "NL",
    "PL",
    "PT",
    "RO",
    "SK",
    "SI",
    "ES",
    "SE",
  ],
  help_docs: {
    livestreaming_data_source_excel: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AYCcJg",
      iframe: "https://test.ipushpull.com/help.php?id=647790593",
    },
    livestreaming_data_source_db: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AYDfC",
      iframe: "https://test.ipushpull.com/help.php?id=148865025",
    },
    livestreaming_data_source_api: {
      desk: "https://ipushpull.atlassian.net/wiki/x/CYCgK",
      iframe: "https://test.ipushpull.com/help.php?id=681607177",
    },
    livestreaming_client_app_ipp: {
      desk: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
      iframe: "https://test.ipushpull.com/help.php?id=681705505",
    },
    livestreaming_client_app_excel: {
      desk: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
      iframe: "https://test.ipushpull.com/help.php?id=156074055",
    },
    livestreaming_client_app_symphony: {
      desk: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
      iframe: "https://test.ipushpull.com/help.php?id=695959587",
    },
    livestreaming_client_app_wp: {
      desk: "https://ipushpull.atlassian.net/wiki/x/a4CBKQ",
      iframe: "https://test.ipushpull.com/help.php?id=696352875",
    },
    livestreaming_client_app_teams: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
      iframe: "https://test.ipushpull.com/help.php?id=678494209",
    },
    notifications_builder_excel_data_source: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AYDmI",
      iframe: "https://test.ipushpull.com/help.php?id=551976961",
    },
    notifications_builder_msteams_webhook: {
      desk: "https://ipushpull.atlassian.net/wiki/x/H4BeHg",
      iframe: "https://test.ipushpull.com/help.php?id=509509663",
    },
    notifications_builder_slack_webhook: {
      desk: "https://ipushpull.atlassian.net/wiki/x/JgBfHg",
      iframe: "https://test.ipushpull.com/help.php?id=509542438",
    },
    notifications_builder_symphony_webhook: {
      desk: "https://ipushpull.atlassian.net/wiki/x/LwBfHg",
      iframe: "https://test.ipushpull.com/help.php?id=509542447",
    },
    symphony_overview: {
      desk: "https://ipushpull.atlassian.net/wiki/x/UYCHDQ",
      iframe: "https://test.ipushpull.com/help.php?id=226984017",
    },
    user_authoriziations: {
      desk: "https://ipushpull.atlassian.net/l/c/GApuWLAA",
      iframe: "https://test.ipushpull.com/help.php?id=631701505",
    },
    page_views: {
      desk: "https://ipushpull.atlassian.net/l/c/sxEHKpxC",
      iframe: "https://test.ipushpull.com/help.php?id=1431175169",
    },
  },
};

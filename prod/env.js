const ENV = {
  ipushpull: {
    api_url: 'https://www.ipushpull.com/api',
    signup_url: 'https://www.ipushpull.com/pricing',
    help_url: 'https://ipushpull.com/contact-support',
    chat_url: 'https://www.ipushpull.com/embeddedapp/integrations/symphony/chat.html',
    api_key: 'LC8aOmGqqydL3n8HyKaz8ue34M13WPux2zyuBAl1',
    api_secret: 'kXXRR7jsoEm6JgjvBDYGca1YlGIDynuxpM9kiu9GTlqzLtmCvNWLm3pmzWUc6Uptp4DwvS1kLlC6mh5PHGGnKRNh0eBPG1IIUB5NtsEY1oSgqLcteNCdGBLnIwnOO7SY',
    storage_prefix: 'ipp',
    hsts: true,    
  },
  clients: {
    ipushpull: {
      app: {
        auth: {
          ipushpull: {
            client_version: 'authapp-2022.1.1'
          },
          images: ['/auth/backgrounds/01.jpg', '/auth/backgrounds/02.jpg']
        },
        client: {
          demo: {
            text: "<h3>Welcome to ipushpull</h3><p>A no-code data-driven workflow platform, delivering your data wherever and whenever it's needed.</p>",
            demo_url: "https://ipushpull.com/123494576",
            demo_btn_text: "Test drive our Interactive Demo",
            video_url: "https://player.vimeo.com/video/513830429?autoplay=1",
            video_btn_text: "Watch our Platform Overview Video" 
          },
          ipushpull: {
            client_version: 'clientapp-2022.1.4'
          },
          lsd: true,
          dod: true,       
          caw: true, 
          caw_popup_url: "https://ipushpull.atlassian.net/l/c/m2TaYu1u",
          stripe: {
            disable: false,
            key: 'pk_live_e0JF1vfg8X6YiIdsdB6vXCzW00SZCHJkQ4'
          },
          theme: {
            upgrade: {
              version: "2022.9.6",
              type: "major",
              release: {
                label: "Contact Support to find out more",
                help_doc: "https://ipushpull.com/contact-support",
              },
              message:
                "<p>There's a new version of the ipushpull app available, which includes:</p><ul><li>Improved usage Dashboard</li><li>Support for clickable links in chatbots and notifications</li><li>Notifications provide better context to support quick decision making</li></ul><p>We have also updated our T&Cs which may be found <a href=http://ipushpull.com/terms/ target=\"_blank\" rel=\"noopener noreferrer\">here</a></p>",
              logo: {
                light: 'https://ipushpull.s3.eu-west-1.amazonaws.com/static/logo-new.svg',
                dark: 'https://ipushpull.s3.eu-west-1.amazonaws.com/static/logo-white.svg'
              },
            },
            ui: {
              admin_sub_organizations: true,
              workspaces_tab_linking: true,
              nav_home_startup: true,
            }
          },
          data_sources: [
            {
              id: 3,
              name: "Excel Add-in",
              key: "excel",
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/excel.png",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://support.ipushpull.com/data-sources/excel-add-in-source/ipushpull-documentation-excel-a-simple-live-push",
                  iframe: "https://support.ipushpull.com/data-sources/excel-add-in-source/ipushpull-documentation-excel-a-simple-live-push",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            },
            {
              id: -1,
              name: "Database",
              key: "db",
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/db.svg",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://support.ipushpull.com/data-sources/data-loader/ipushpull-documentation-setting-up-a-push-from-your-database-to-ipushpull",
                  iframe: "https://support.ipushpull.com/data-sources/data-loader/ipushpull-documentation-setting-up-a-push-from-your-database-to-ipushpull",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            },
            {
              id: -1,
              name: "API",
              key: "api",
              logo: "",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://support.ipushpull.com/data-sources/ipushpull-documentation-ipushpull-api",
                  iframe: "https://support.ipushpull.com/data-sources/ipushpull-documentation-ipushpull-api",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            }
          ],
          client_apps: [
            {
              id: 2,
              name: "Desktop App",
              key: "ipp",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
              help: {
                lsd: {
                  desk: "https://support.ipushpull.com/client-integrations/ipushpull-web-application/ipushpull-documentation-pull-live-and-streaming-data-into-the-ipushpull-client-app",
                  iframe: "https://support.ipushpull.com/client-integrations/ipushpull-web-application/ipushpull-documentation-pull-live-and-streaming-data-into-the-ipushpull-client-app",
                },
                dod: {
                  desk: "",
                  iframe: ""
                }
              },
              appType: ['lsd']
            },
            {
              id: 41,
              name: "Mobile App",
              key: "mobile",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://support.ipushpull.com/client-integrations/ipushpull-mobile-app/ipushpull-documentation-mobile-devices-ios-and-android",
                  iframe: "https://support.ipushpull.com/client-integrations/ipushpull-mobile-app/ipushpull-documentation-mobile-devices-ios-and-android",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            },
            {
              id: 3,
              name: "Excel Add-in",
              key: "excel",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/excel.png",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://support.ipushpull.com/client-integrations/excel-add-in-client/ipushpull-documentation-excel-a-simple-live-pull",
                  iframe: "https://support.ipushpull.com/client-integrations/excel-add-in-client/ipushpull-documentation-excel-a-simple-live-pull",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            },
            {
              id: 38,
              name: "Microsoft Teams App",
              key: "teams",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/teams.svg",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://support.ipushpull.com/client-integrations/microsoft-teams/ipushpull-documentation-pull-live-and-streaming-data-into-microsoft-teams",
                  iframe: "https://support.ipushpull.com/client-integrations/microsoft-teams/ipushpull-documentation-pull-live-and-streaming-data-into-microsoft-teams",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            },
            {
              id: 42,
              name: "Microsoft Teams Bot",
              key: "teamsBot",
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/teams.svg",
              appType: ['dod'],
              help: {
                lsd: {
                  desk: "",
                  iframe: "",
                },
                dod: {
                  desk: "https://support.ipushpull.com/client-integrations/microsoft-teams/ipushpull-documentation-using-the-ipushpull-microsoft-teams-bot",
                  iframe: "https://support.ipushpull.com/client-integrations/microsoft-teams/ipushpull-documentation-using-the-ipushpull-microsoft-teams-bot",
                }
              }
            },
            {
              id: 7,
              name: "Slack Bot",
              key: "slack",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/slack.png",
              appType: ['dod'],
              help: {
                lsd: {
                  desk: "",
                  iframe: "",
                },
                dod: {
                  desk: "https://support.ipushpull.com/client-integrations/slack/ipushpull-documentation-using-the-ipushpull-slack-app",
                  iframe: "https://support.ipushpull.com/client-integrations/slack/ipushpull-documentation-using-the-ipushpull-slack-app",
                }
              }
            },
            {
              id: 14,
              name: "Symphony App",
              key: "symphony",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/symphony.png",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://support.ipushpull.com/client-integrations/symphony/ipushpull-documentation-pull-live-and-streaming-data-into-symphony",
                  iframe: "https://support.ipushpull.com/client-integrations/symphony/ipushpull-documentation-pull-live-and-streaming-data-into-symphony",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            },
            {
              id: 43,
              name: "Symphony Bot",
              key: "symphonyBot",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/symphony.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
              appType: ['dod'],
              help: {
                lsd: {
                  desk: "",
                  iframe: "",
                },
                dod: {
                  desk: "https://support.ipushpull.com/client-integrations/symphony/using-the-ipushpull-chatbot-in-symphony",
                  iframe: "https://support.ipushpull.com/client-integrations/symphony/using-the-ipushpull-chatbot-in-symphony",
                }
              }
            },
            {
              id: 35,
              name: "TaDa",
              key: "tada",
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/ETD_Symphonylogo.png",
              appType: ['lsd','dod'],
              help: {
                lsd: {
                  desk: "https://ipushpull.gitbook.io/euromoney-tradedata-tada-guide/getting-started",
                  iframe: "https://ipushpull.gitbook.io/euromoney-tradedata-tada-guide/getting-started",
                },
                dod: {
                  desk: "https://ipushpull.gitbook.io/euromoney-tradedata-tada-guide/getting-started",
                  iframe: "https://ipushpull.gitbook.io/euromoney-tradedata-tada-guide/getting-started"
                }
              }
            },
            {
              id: -1,
              name: "Client API",
              key: "api",
              logo: "",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://support.ipushpull.com/data-sources/ipushpull-documentation-ipushpull-api",
                  iframe: "https://support.ipushpull.com/data-sources/ipushpull-documentation-ipushpull-api",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            }
          ],          
        },
        embed: {
          ipushpull: {
            client_version: 'embedapp-2021.3.2'
          }
        },
        mobile: {
          ipushpull: {
            storage_prefix: "ipp_mobile",
            client_version: "mobile-2021.3.2",
            api_key: "adyDC4L5hle5MOt6zZoylVX9Sm7HiAwPAcU86fON",
            api_secret:
              "k0tsTQLLbo7UJ8hVJM0uI9ljtFDb6IVxmpLATgI5uuzRm2s01RB4dmr8v61xBCbpRqq8jb7bWEj8sn5mQP7xRNY0PyY4O6OgcCvjVjMpg1Z6XqMxmn3zu1FK3cah84ld",
          },
          client_application: "mobile",
        },        
        symphony: {
          destinations: true,
          disable_email_destinations: true,
          plugins: true,
          client_application: 'symphony-embedded-app',
          symphony_streams: true,          
          ipushpull: {
            client_version: 'syapp-2021.3.2',
            storage_prefix: 'ipp_symphony',
            api_key: 'ZgRsNG7mkLTNupSS1hg11PQgVHR4GWQAK2GOgFWv',
            api_secret: 'pziee5N2TppBTSZISzfixSk8yvkiCGAaptB8emv88a0ETfJ7IP5GY8515gPE3aeVFmwMzKNiMD416eDqwC5Skt2zHpudHGghg0nedAyUBRIsPfdbn42c4SHOys58kSy3'
          },
          workflow: {
            novation: true,
          },
          about: {
            demo: "",
            more_info:
              "https://www.ipushpull.com/client-symphony?ref=symphonyapp",
          },          
          services: [
            {
              title: "Live & Streaming Data",
              description:
                "Deliver real-time data into Symphony and other desktop and mobile apps for your team or clients",
              class: "border-color-ipp-secondary",
              color: "ipp-secondary",
              icon: "table-large",
              more_info: "https://ipushpull.com/service-live-streaming",
            },
            {
              title: "Data on Demand",
              description:
                "Pull data on demand from live systems into Symphony, or send updates with ipushpull's configurable bot service",
              class: "border-color-ipp-tertiary",
              color: "ipp-tertiary",
              icon: "robot",
              more_info: "https://ipushpull.com/service-data-on-demand",
            },
            {
              title: "Data-driven Notifications",
              description:
                "Automated, live data-driven alerts sent to your teams or clients on desktop and mobile apps",
              class: "border-color-ipp-primary",
              color: "ipp-primary",
              icon: "bell",
              more_info: "https://ipushpull.com/service-notifications",
            },
            {
              title: "Custom Applications & Workflows",
              description:
                "Customised solutions and workflows, integrated and white-labelled to order",
              class: "border-color-ipp-yellow",
              color: "ipp-yellow",
              icon: "flow-chart",
              more_info: "https://ipushpull.com/service-custom-applications",
            },
          ],          
          integration: {
            id: "ipushpull",
            ignoreIdParam: true,
            premium: true,
            cookie: {
              prefix: "ipp_symphony",
              oauth_access_token: "access_token",
              oauth_refresh_token: "refresh_token",
              uuid: "uuid"
            },
            urls: {
              api: "https://www.ipushpull.com/api/1.0",
              app: "https://www.ipushpull.com/syapp/",
              docs: "https://www.ipushpull.com/docs/",
              module: "https://www.ipushpull.com/embeddedapp/integrations/symphony/app-premium.html",
              blank: "https://www.ipushpull.com/embeddedapp/integrations/symphony/blank.html"
            },
            icons: {
              ui: "https://ipushpull.s3.amazonaws.com/static/prd/icon-32.png",
              nav: "https://ipushpull.s3.amazonaws.com/static/prd/icon-32.png",
            },
          }
        },
        finsemble: {
          ipushpull: {
            storage_prefix: 'ipp_finsemble',
            api_key: 'ZgRsNG7mkLTNupSS1hg11PQgVHR4GWQAK2GOgFWv',
            api_secret: 'pziee5N2TppBTSZISzfixSk8yvkiCGAaptB8emv88a0ETfJ7IP5GY8515gPE3aeVFmwMzKNiMD416eDqwC5Skt2zHpudHGghg0nedAyUBRIsPfdbn42c4SHOys58kSy3'
          },
          plugins: true
        },
        teams: {
          plugins: true,
          client_application: "ipp-teams-app",
          more_info_url: "https://support.ipushpull.com",          
          ipushpull: {
            help_url: "https://support.ipushpull.com/teams",
            storage_prefix: "ipp_teams",
            api_key: "3czdG4X2my5ZhxC0z5RyG06dllC4vEjA7hlV8KDT",
            api_secret: "jVCZpGPvG8JC3tGYJZBYXlnuyWk6eeHS4PctbsP7bxMcIOkWIafbT9gtjuzXR0rK2uPrB9iTu0rWnNBcDWMXqUZRRuKJOGIjvolJGvNelRAiEbz1OEWtOn7S6PKxDOUB",
          },
          guides: [
            { version: '2021.3.2', name: 'finder', content: 'Click here to choose a live data page', position: 'bottom' },
            { version: '2021.3.2', name: 'toolbar', content: 'Filter and sort your data and change settings here', position: 'bottom' },
            { version: '2021.3.2', name: 'footer', content: 'For help and support choose an option', position: 'top' },
          ],         
          integration: {
            urls: {
              app: "https://www.ipushpull.com/roapp/",
              module: "https://www.ipushpull.com/integrations/teams/app.html",
              remove: "https://www.ipushpull.com/integrations/teams/tabremove.html",
              logout: "https://www.ipushpull.com/integrations/teams/tabpersonal.html",
            }
          },
        },
      },
    },
    euromoney: {
      app: {
        symphony: {
          hide_route: true,
          __hide_page_border: true,
          plugins: true,
          client_application: "euromoney-syapp",
          symphony_streams: true,          
          ipushpull: {
            help_url: "",
            storage_prefix: "euromoney_prod_symphony",
            client_version: "symphony-prod",
            api_key: "x9NSN0oV9zNyHO3ZuEJlV1n7mN51RpMahfrzfyhJ",
            api_secret: "jpVvTCpaxQmbQ5uCLEbJT10J3pbP4OlGM19nTDR5t1z79WEejcvRBRXSCzQmGxio4WlKnzew80KYhSgmfkSS5kbSnyS1k4TX89mUYp9hpPwXKkiF5P5sV2nivjDQBL1e",
          },
          destinations: true,
          disable_email_destinations: true,
          message_icon: "https://ipushpull.s3.amazonaws.com/static/prd/icon-32.png",
          share_icon: "https://ipushpull.s3.amazonaws.com/static/prd/ipushpull-thumb.png",
          integration: {
            id: "euromoney-prod",
            title: "Euromoney TRADEDATA",
            name: "Euromoney TRADEDATA",
            urls: {
              app: "https://www.ipushpull.com/roapp/",
              module: "https://www.ipushpull.com/embeddedapp/integrations/symphony/app.html",
              blank: "https://www.ipushpull.com/embeddedapp/integrations/symphony/blank.html",
            },
            icons: {
              ui: "https://ipushpull.s3.amazonaws.com/static/prd/EMTD_icon_32x16.png",
              nav: "https://ipushpull.s3.amazonaws.com/static/prd/EMTD_icon_32x32.png",
            },
          },
          theme: {
            color: {
              primary: '#361744',
              secondary: '#8a70a0'
            },
            header: {
              icon: {
                light: 'https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-square-light.svg',
                dark: 'https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-square-dark.svg',
              }
            },
            home: {
              logo: {
                light: 'https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-light.svg',
                dark: 'https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-dark.svg'
              },
            }
          },
          tos_url: "https://www.euromoneytradedata.com/terms-and-conditions",
          privacy_url: "https://www.euromoneytradedata.com/privacy-policy",
          more_info_url: "https://www.euromoneytradedata.com/"
        },
        teams: {
          ipushpull: {
            storage_prefix: "euromoney_prod_teams",
            client_version: "teams-prod",
            help_url: "",
            api_key: "Qt9722tFkP4CavE4gZXH19pSiXyeSMQcR5p29dqC",
            api_secret: "Ojahg4E6rxh9cGOuTl8lAddP1ost0Rkj4iMPfyJvWQeLBZrinbG3OBECMc0RgBjCsoAZe2fvNUC4TlnjWqBYf3jJDvgk9obDMikmSvxt9iDpmA8i3QPxsPVXmz08B5fq",
          },
          integration: {
            urls: {
              app: "https://www.ipushpull.com/roapp/",
              module: "https://www.ipushpull.com/embeddedapp/integrations/teams/app.html?client=euromoney",
              remove: "https://www.ipushpull.com/embeddedapp/integrations/teams/tabremove.html",
            },
          },
          theme: {
            title: 'Euromoney TRADEDATA',
            color: {
              primary: '#361744',
              secondary: '#8a70a0'
            },
            header: {
              icon: {
                light: 'https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-square-light.svg',
                dark: 'https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-square-dark.svg',
              }
            },
            home: {
              logo: {
                light: 'https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-light.svg',
                dark: 'https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-dark.svg'
              },
            }
          },
          client_application: "euromoney-teams-app",
          tos_url: "https://www.euromoneytradedata.com/terms-and-conditions",
          privacy_url: "https://www.euromoneytradedata.com/privacy-policy",
          more_info_url: "https://www.euromoneytradedata.com/"
        },
      },
    },
  },
  app: {
    auth: {
      ipushpull: {},
      images: ['/auth/backgrounds/01.jpg', '/auth/backgrounds/02.jpg']
    },
    client: {
      ipushpull: {
        storage_prefix: 'ipp_local',
        client_version: 'client-12345'
      },
      stripe: {
        disable: false,
        key: 'pk_live_sGiB7dp7FnpCaOVsnkoGv62Y'
      }
    },
    embed: {
      ipushpull: {
        client_version: 'embedapp-2.0.0'
      }
    },
    symphony: {
      destinations: true,
      disable_email_destinations: true,
      ipushpull: {
        storage_prefix: 'ipp_symphony',
        client_version: 'symphony-12345'
      },
      message_icon: 'https://ipushpull.s3.amazonaws.com/static/prd/icon-32.png',
      share_icon: 'https://ipushpull.s3.amazonaws.com/static/prd/ipushpull-thumb.png'
    }
  },
  max_upload_size: 200,
  billing: true,
  eu_vat_countries: [
    'AT',
    'BE',
    'BG',
    'HR',
    'CY',
    'CZ',
    'DK',
    'EE',
    'FI',
    'FR',
    'DE',
    'EL',
    'HU',
    'IE',
    'IT',
    'LV',
    'LT',
    'LU',
    'MT',
    'NL',
    'PL',
    'PT',
    'RO',
    'SK',
    'SI',
    'ES',
    'SE'
  ],
  help_docs: {
    notifications_troubleshooting: {
      desk: "https://support.ipushpull.com/data-services/ipushpull-documentation-data-driven-notifications/ipushpull-documentation-troubleshooting-notifications-problems",
      iframe: "https://support.ipushpull.com/data-services/ipushpull-documentation-data-driven-notifications/ipushpull-documentation-troubleshooting-notifications-problems",
    },
    notifications_builder_excel_data_source: {
      desk: "https://support.ipushpull.com/data-sources/excel-add-in-source/ipushpull-documentation-getting-started-how-to-send-data-notifications-from-excel",
      iframe: "https://support.ipushpull.com/data-sources/excel-add-in-source/ipushpull-documentation-getting-started-how-to-send-data-notifications-from-excel",
    },
    notifications_builder_msteams_webhook: {
      desk: "https://support.ipushpull.com/client-integrations/microsoft-teams/ipushpull-documentation-create-a-microsoft-teams-incoming-webhook",
      iframe: "https://support.ipushpull.com/client-integrations/microsoft-teams/ipushpull-documentation-create-a-microsoft-teams-incoming-webhook",
    },
    notifications_builder_slack_webhook: {
      desk: "https://support.ipushpull.com/client-integrations/slack/ipushpull-documentation-create-a-slack-incoming-webhook",
      iframe: "https://support.ipushpull.com/client-integrations/slack/ipushpull-documentation-create-a-slack-incoming-webhook",
    },
  },
};
window.hsConversationsSettings = {
  loadImmediately: false,
};
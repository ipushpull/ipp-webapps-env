const ENV = {
  ipushpull: {
    api_url: 'https://www.ipushpull.com/api',
    signup_url: 'https://www.ipushpull.com/pricing',
    help_url: 'https://support.ipushpull.com',
    chat_url: 'https://www.ipushpull.com/embeddedapp/integrations/symphony/chat.html',
    api_key: 'LC8aOmGqqydL3n8HyKaz8ue34M13WPux2zyuBAl1',
    api_secret: 'kXXRR7jsoEm6JgjvBDYGca1YlGIDynuxpM9kiu9GTlqzLtmCvNWLm3pmzWUc6Uptp4DwvS1kLlC6mh5PHGGnKRNh0eBPG1IIUB5NtsEY1oSgqLcteNCdGBLnIwnOO7SY',
    storage_prefix: 'ipp',
    hsts: false,    
  },
  clients: {
    ipushpull: {
      app: {
        auth: {
          ipushpull: {
            client_version: 'authapp-2021.3.2'
          },
          images: ['/auth/backgrounds/01.jpg', '/auth/backgrounds/02.jpg']
        },
        client: {
          ipushpull: {
            client_version: 'clientapp-2021.6.4'
          },
          lsd: true,
          dod: true,          
          stripe: {
            disable: false,
            key: 'pk_live_e0JF1vfg8X6YiIdsdB6vXCzW00SZCHJkQ4'
          },
          theme: {
            upgrade: {
              version: "2021.6.4",
              type: "major",
              release: {
                label: "Find out more",
                help_doc: "https://ipushpull.atlassian.net/l/c/MoyVFwSc"
              },              
              message: "<p>There's a new version of the ipushpull app available, which includes:</p><ul><li>Expanded Notification Service Wizards</li><li>View management in Symphony App</li></ul>",
              logo: {
                light: 'https://ipushpull.s3.eu-west-1.amazonaws.com/static/logo-new.svg',
                dark: 'https://ipushpull.s3.eu-west-1.amazonaws.com/static/logo-white.svg'
              },
            }              
          },
          services: [
            {
              name: "Desktop",
              key: "ipp",
              serviceType: ['lsd'],
              logo:
              "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
            },
            {
              name: "Mobile",
              key: "mobile",
              serviceType: ['lsd'],
              logo:
              "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
            },
            {
              name: "Excel Add-in",
              key: "excel",
              serviceType: ['lsd'],
              logo:
              "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/excel.png",
            },
            {
              name: "Client API",
              key: "api",
              serviceType: ['lsd'],
            },
            {
              name: "Microsoft Teams",
              key: "teams",
              botKey: "teamsBot",
              serviceType: ['lsd', 'dod', 'ddn'],
              logo:
              "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/teams.png",
            },
            {
              name: "Symphony",
              key: "symphony",
              botKey: "symphonyBot",
              serviceType: ['lsd', 'dod', 'ddn'],
              logo:
              "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/symphony.png",
            },
            {
              name: "Slack",
              key: "slack",
              serviceType: ['dod', 'ddn'],
              logo:
              "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/slack.png",
            },
            {
              name: "WhatsApp",
              key: "whatsApp",
              serviceType: ['ddn'],
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/whatsapp.png",
            },
            {
              name: "SMS",
              key: "sms",
              serviceType: ['ddn'],
              logo:
              "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/sms.png",
            },
            {
              name: "Email",
              key: "email",
              serviceType: ['ddn'],
              logo:
              "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/email.png",
            },
          ],
          data_sources: [
            {
              id: 2,
              name: "Excel Add-in",
              key: "excel",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/excel.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/AYCcJg",
                  iframe: "https://test.ipushpull.com/help.php?id=647790593",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            },
            {
              id: -1,
              name: "Database",
              key: "db",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/db.svg",
              // help_doc: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/AYDfC",
                  iframe: "https://test.ipushpull.com/help.php?id=148865025",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            },
            {
              id: -1,
              name: "API",
              key: "api",
              logo: "",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/l/c/xsgmc85x",
                  iframe: "https://test.ipushpull.com/help.php?id=681607177",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            }
          ],
          client_apps: [
            {
              id: 3,
              name: "Desktop App",
              key: "ipp",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
                  iframe: "https://test.ipushpull.com/help.php?id=681705505",
                },
                dod: {
                  desk: "",
                  iframe: ""
                }
              },
              appType: ['lsd']
            },
            {
              id: 1,
              name: "Mobile App",
              key: "mobile",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/l/c/qxv9HZC1",
                  iframe: "https://test.ipushpull.com/help.php?id=650248196",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            },
            {
              id: 2,
              name: "Excel Add-in",
              key: "excel",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/excel.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
                  iframe: "https://test.ipushpull.com/help.php?id=156074055",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            },
            {
              id: 30,
              name: "Microsoft Teams App",
              key: "teams",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/teams.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
                  iframe: "https://test.ipushpull.com/help.php?id=678494209",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/aut0u3UB",
                  iframe: "https://test.ipushpull.com/help.php?id=741670913",
                }
              }
            },
            {
              id: 31,
              name: "Microsoft Teams Bot",
              key: "teamsBot",
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/teams.png",
              appType: ['dod'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
                  iframe: "https://test.ipushpull.com/help.php?id=678494209",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/aut0u3UB",
                  iframe: "https://test.ipushpull.com/help.php?id=741670913",
                }
              }
            },
            {
              id: 29,
              name: "Slack Bot",
              key: "slack",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/slack.png",
              help_doc: "https://ipushpull.atlassian.net/l/c/3DvJko2W",
              appType: ['dod'],
              help: {
                lsd: {
                  desk: "",
                  iframe: "",
                },
                dod: {
                  desk: "https://slack.com/oauth/v2/authorize?client_id=2670085612.85257602438&scope=app_mentions:read,channels:read,chat:write,commands,im:history,im:read,users:read&redirect_uri=https%3a%2f%2fslack.botframework.com%2fHome%2fauth%2fv2&state=ipushpull",
                  iframe: "https://test.ipushpull.com/help.php?id=1627553793",
                }
              }
            },
            {
              id: 11,
              name: "Symphony App",
              key: "symphony",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/symphony.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
                  iframe: "https://test.ipushpull.com/help.php?id=695959587",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/DeKPs5es",
                  iframe: "https://test.ipushpull.com/help.php?id=742162433",
                }
              }
            },
            {
              id: 32,
              name: "Symphony Bot",
              key: "symphonyBot",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/symphony.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
              appType: ['dod'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
                  iframe: "https://test.ipushpull.com/help.php?id=695959587",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/DeKPs5es",
                  iframe: "https://test.ipushpull.com/help.php?id=742162433",
                }
              }
            },
            {
              id: -1,
              name: "Client API",
              key: "api",
              logo: "",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/l/c/xsgmc85x",
                  iframe: "https://test.ipushpull.com/help.php?id=681607177",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            }
          ],                   
          lsd_apps: [
            {
              id: 3,
              name: "Desktop app",
              key: "ipp",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
            },
            {
              id: 1,
              name: "Mobile app",
              key: "mobile",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
            },            
            {
              id: 2,
              name: "Excel Add-in",
              key: "excel",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/excel.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
            },
            {
              id: 30,
              name: "Microsoft Teams",
              key: "teams",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/teams.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
            },
            {
              id: 11,
              name: "Symphony",
              key: "symphony",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/symphony.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
            }
          ],          
        },
        embed: {
          ipushpull: {
            client_version: 'embedapp-2021.3.2'
          }
        },
        mobile: {
          ipushpull: {
            storage_prefix: "ipp_mobile",
            client_version: "mobile-2021.3.2",
            api_key: "adyDC4L5hle5MOt6zZoylVX9Sm7HiAwPAcU86fON",
            api_secret:
              "k0tsTQLLbo7UJ8hVJM0uI9ljtFDb6IVxmpLATgI5uuzRm2s01RB4dmr8v61xBCbpRqq8jb7bWEj8sn5mQP7xRNY0PyY4O6OgcCvjVjMpg1Z6XqMxmn3zu1FK3cah84ld",
          },
          client_application: "mobile",
        },        
        symphony: {
          destinations: true,
          disable_email_destinations: true,
          plugins: true,
          client_application: 'symphony-embedded-app',
          ipushpull: {
            client_version: 'syapp-2021.3.2',
            storage_prefix: 'ipp_symphony',
            api_key: 'ZgRsNG7mkLTNupSS1hg11PQgVHR4GWQAK2GOgFWv',
            api_secret: 'pziee5N2TppBTSZISzfixSk8yvkiCGAaptB8emv88a0ETfJ7IP5GY8515gPE3aeVFmwMzKNiMD416eDqwC5Skt2zHpudHGghg0nedAyUBRIsPfdbn42c4SHOys58kSy3'
          },
          integration: {
            id: "ipushpull",
            ignoreIdParam: true,
            premium: true,
            cookie: {
              prefix: "ipp_symphony",
              oauth_access_token: "access_token",
              oauth_refresh_token: "refresh_token",
              uuid: "uuid"
            },
            urls: {
              api: "https://www.ipushpull.com/api/1.0",
              app: "https://www.ipushpull.com/syapp/",
              docs: "https://www.ipushpull.com/docs/",
              module: "https://www.ipushpull.com/embeddedapp/integrations/symphony/app-premium.html",
              blank: "https://www.ipushpull.com/embeddedapp/integrations/symphony/blank.html"
            },
            icons: {
              ui: "https://ipushpull.s3.amazonaws.com/static/prd/icon-32.png",
              nav: "https://ipushpull.s3.amazonaws.com/static/prd/icon-32.png",
            },
          }
        },
        finsemble: {
          ipushpull: {
            storage_prefix: 'ipp_finsemble',
            api_key: 'ZgRsNG7mkLTNupSS1hg11PQgVHR4GWQAK2GOgFWv',
            api_secret: 'pziee5N2TppBTSZISzfixSk8yvkiCGAaptB8emv88a0ETfJ7IP5GY8515gPE3aeVFmwMzKNiMD416eDqwC5Skt2zHpudHGghg0nedAyUBRIsPfdbn42c4SHOys58kSy3'
          },
          plugins: true
        },
        teams: {
          plugins: true,
          client_application: "ipp-teams-app",
          more_info_url: "https://support.ipushpull.com",          
          ipushpull: {
            help_url: "https://support.ipushpull.com/teams",
            storage_prefix: "ipp_teams",
            api_key: "3czdG4X2my5ZhxC0z5RyG06dllC4vEjA7hlV8KDT",
            api_secret: "jVCZpGPvG8JC3tGYJZBYXlnuyWk6eeHS4PctbsP7bxMcIOkWIafbT9gtjuzXR0rK2uPrB9iTu0rWnNBcDWMXqUZRRuKJOGIjvolJGvNelRAiEbz1OEWtOn7S6PKxDOUB",
          },
          guides: [
            { version: '2021.3.2', name: 'finder', content: 'Click here to choose a live data page', position: 'bottom' },
            { version: '2021.3.2', name: 'toolbar', content: 'Filter and sort your data and change settings here', position: 'bottom' },
            { version: '2021.3.2', name: 'footer', content: 'For help and support choose an option', position: 'top' },
          ],         
          integration: {
            urls: {
              app: "https://www.ipushpull.com/roapp/",
              module: "https://www.ipushpull.com/integrations/teams/app.html",
              remove: "https://www.ipushpull.com/integrations/teams/tabremove.html",
              logout: "https://www.ipushpull.com/integrations/teams/tabpersonal.html",
            }
          },
        },
      },
    },
    euromoney: {
      app: {
        symphony: {
          hide_route: true,
          __hide_page_border: true,
          plugins: true,
          client_application: "euromoney-syapp",
          ipushpull: {
            help_url: "",
            storage_prefix: "euromoney_prod_symphony",
            client_version: "symphony-prod",
            api_key: "x9NSN0oV9zNyHO3ZuEJlV1n7mN51RpMahfrzfyhJ",
            api_secret: "jpVvTCpaxQmbQ5uCLEbJT10J3pbP4OlGM19nTDR5t1z79WEejcvRBRXSCzQmGxio4WlKnzew80KYhSgmfkSS5kbSnyS1k4TX89mUYp9hpPwXKkiF5P5sV2nivjDQBL1e",
          },
          destinations: true,
          disable_email_destinations: true,
          message_icon: "https://ipushpull.s3.amazonaws.com/static/prd/icon-32.png",
          share_icon: "https://ipushpull.s3.amazonaws.com/static/prd/ipushpull-thumb.png",
          integration: {
            id: "euromoney-prod",
            title: "Euromoney TRADEDATA",
            name: "Euromoney TRADEDATA",
            urls: {
              app: "https://www.ipushpull.com/roapp/",
              module: "https://www.ipushpull.com/embeddedapp/integrations/symphony/app.html",
              blank: "https://www.ipushpull.com/embeddedapp/integrations/symphony/blank.html",
            },
            icons: {
              ui: "https://ipushpull.s3.amazonaws.com/static/prd/EMTD_icon_32x16.png",
              nav: "https://ipushpull.s3.amazonaws.com/static/prd/EMTD_icon_32x32.png",
            },
          },
          theme: {
            color: {
              primary: '#361744',
              secondary: '#8a70a0'
            },
            header: {
              icon: {
                light: 'https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-square-light.svg',
                dark: 'https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-square-dark.svg',
              }
            },
            home: {
              logo: {
                light: 'https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-light.svg',
                dark: 'https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-dark.svg'
              },
            }
          },
          tos_url: "https://www.euromoneytradedata.com/terms-and-conditions",
          privacy_url: "https://www.euromoneytradedata.com/privacy-policy",
          more_info_url: "https://www.euromoneytradedata.com/"
        },
        teams: {
          ipushpull: {
            storage_prefix: "euromoney_prod_teams",
            client_version: "teams-prod",
            help_url: "",
            api_key: "Qt9722tFkP4CavE4gZXH19pSiXyeSMQcR5p29dqC",
            api_secret: "Ojahg4E6rxh9cGOuTl8lAddP1ost0Rkj4iMPfyJvWQeLBZrinbG3OBECMc0RgBjCsoAZe2fvNUC4TlnjWqBYf3jJDvgk9obDMikmSvxt9iDpmA8i3QPxsPVXmz08B5fq",
          },
          integration: {
            urls: {
              app: "https://www.ipushpull.com/roapp/",
              module: "https://www.ipushpull.com/embeddedapp/integrations/teams/app.html?client=euromoney",
              remove: "https://www.ipushpull.com/embeddedapp/integrations/teams/tabremove.html",
            },
          },
          theme: {
            title: 'Euromoney TRADEDATA',
            color: {
              primary: '#361744',
              secondary: '#8a70a0'
            },
            header: {
              icon: {
                light: 'https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-square-light.svg',
                dark: 'https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-square-dark.svg',
              }
            },
            home: {
              logo: {
                light: 'https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-light.svg',
                dark: 'https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-dark.svg'
              },
            }
          },
          client_application: "euromoney-teams-app",
          tos_url: "https://www.euromoneytradedata.com/terms-and-conditions",
          privacy_url: "https://www.euromoneytradedata.com/privacy-policy",
          more_info_url: "https://www.euromoneytradedata.com/"
        },
      },
    },
  },
  app: {
    auth: {
      ipushpull: {},
      images: ['/auth/backgrounds/01.jpg', '/auth/backgrounds/02.jpg']
    },
    client: {
      ipushpull: {
        storage_prefix: 'ipp_local',
        client_version: 'client-12345'
      },
      stripe: {
        disable: false,
        key: 'pk_live_sGiB7dp7FnpCaOVsnkoGv62Y'
      }
    },
    embed: {
      ipushpull: {
        client_version: 'embedapp-2.0.0'
      }
    },
    symphony: {
      destinations: true,
      disable_email_destinations: true,
      ipushpull: {
        storage_prefix: 'ipp_symphony',
        client_version: 'symphony-12345'
      },
      message_icon: 'https://ipushpull.s3.amazonaws.com/static/prd/icon-32.png',
      share_icon: 'https://ipushpull.s3.amazonaws.com/static/prd/ipushpull-thumb.png'
    }
  },
  max_upload_size: 200,
  billing: true,
  eu_vat_countries: [
    'AT',
    'BE',
    'BG',
    'HR',
    'CY',
    'CZ',
    'DK',
    'EE',
    'FI',
    'FR',
    'DE',
    'EL',
    'HU',
    'IE',
    'IT',
    'LV',
    'LT',
    'LU',
    'MT',
    'NL',
    'PL',
    'PT',
    'RO',
    'SK',
    'SI',
    'ES',
    'SE'
  ],
  help_docs: {
    livestreaming_data_source_excel: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AYCcJg",
      iframe: "https://www.ipushpull.com/help.php?id=647790593",
    },
    livestreaming_data_source_db: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AYDfC",
      iframe: "https://www.ipushpull.com/help.php?id=148865025"
    },
    livestreaming_data_source_api: {
      desk: "https://ipushpull.atlassian.net/wiki/x/CYCgK",
      iframe: "https://www.ipushpull.com/help.php?id=681607177",
    },
    livestreaming_client_app_ipp: {
      desk: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
      iframe: "https://www.ipushpull.com/help.php?id=681705505",
    },
    livestreaming_client_app_excel: {
      desk: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
      iframe: "https://www.ipushpull.com/help.php?id=156074055",
    },
    livestreaming_client_app_symphony: {
      desk: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
      iframe: "https://www.ipushpull.com/help.php?id=695959587",
    },
    livestreaming_client_app_wp: {
      desk: "https://ipushpull.atlassian.net/wiki/x/a4CBKQ",
      iframe: "https://www.ipushpull.com/help.php?id=696352875",
    },
    livestreaming_client_app_teams: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
      iframe: "https://www.ipushpull.com/help.php?id=678494209",
    },
    notifications_builder_excel_data_source: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AYDmI",
      iframe: "https://www.ipushpull.com/help.php?id=502628353",
    },
    notifications_builder_msteams_webhook: {
      desk: "https://ipushpull.atlassian.net/wiki/x/H4BeHg",
      iframe: "https://www.ipushpull.com/help.php?id=509509663",
    },
    notifications_builder_slack_webhook: {
      desk: "https://ipushpull.atlassian.net/wiki/x/JgBfHg",
      iframe: "https://www.ipushpull.com/help.php?id=509542438",
    },
    notifications_builder_symphony_webhook: {
      desk: "https://ipushpull.atlassian.net/wiki/x/LwBfHg",
      iframe: "https://www.ipushpull.com/help.php?id=509542447",
    },
    symphony_overview: {
      desk: "https://ipushpull.atlassian.net/wiki/x/UYCHDQ",
      iframe: "https://www.ipushpull.com/help.php?id=226984017",
    },
    user_authoriziations: {
      desk: "https://ipushpull.atlassian.net/l/c/GApuWLAA",
      iframe: "https://test.ipushpull.com/help.php?id=631701505",
    }
  },
};
window.hsConversationsSettings = {
  loadImmediately: false,
};
const ENV = {
  ipushpull: {
    api_url: "https://uat-trayport.ipushpull.com/api",
    auth_url: "https://release-2022-10-1.djp9f5rksyz8p.amplifyapp.com/auth",
    app_url: "https://release-2022-10-1.djp9f5rksyz8p.amplifyapp.com",
    api_key: "Dziq3emLyhQKwVQYLLlxfiDzoTpcm2znpFG0RaFD",
    api_secret: "CxhNIt0l4351AOxSDE9HB4MMVi4VGXtncJxQShpGVB0D0C5rj2aiElRFIKVzrwpb5xR2Zzmwnpj2fEoAVcczgHP2Gmy4qm9Hdnem8zpWJTYRDAsecNcZSAE8EgIebAJP",
    hsts: false,
    storage_prefix: "test-trayport-webapp",
    transport: "polling",
    client_version: "",
  },
  clients: {
    ipushpull: {
      app: {
        auth: {
          ipushpull: {
            client_version: "auth",
          },
          images: [],
        },
        embed: {
          ipushpull: {
            client_version: "embed",
          },
        },
        client: {
          ipushpull: {
            client_version: "client",
          },
          stripe: {
            key: "",
          },
          dod: true,
          theme: {
            upgrade: {
              version: "2022.11.1",
              type: "minor",
              release: {
                label: "Find out more",
                help_doc: "https://support.ipushpull.com",
              },
              message:
                "<p>There's a new version of the ipushpull app available, which includes:</p><ul><li>Improved usage Dashboard</li><li>Support for clickable links in chatbots and notifications</li><li>Notifications provide better context to support quick decision making</li></ul><p>We have also updated our T&Cs which may be found <a href=http://ipushpull.com/terms/ target=\"_blank\" rel=\"noopener noreferrer\">here</a></p>",
              logo: {
                light:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/static/logo-new.svg",
                dark:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/static/logo-white.svg",
              },
            },
            ui: {
              nav_workspaces: true,
              nav_folders: true,
              nav_create: true,
              nav_help: true,
              nav_share: true,
              nav_setup: true,
              nav_setup_lsd: true,
              nav_setup_dod: true,
              nav_setup_ddn: true,
              nav_setup_caw: true,
              nav_setup_admin: true,

              profile_tab_integrations: true,
              profile_tab_keys: true,

              folders_and_pages_types: [],
              folders_and_pages_can_pin_folder: true,
              folders_and_pages_can_leave_folder: true,
              folders_and_pages_tab_recent: true,
              folders_and_pages_tab_favs: true,

              toolbar: true,
              toolbar_fav: true,
              toolbar_autosave: true,
              toolbar_columns: true,
              toolbar_highlights: true,
              toolbar_tracking: true,
              toolbar_filters: true,
              toolbar_sort: true,
              toolbar_views: true,
              toolbar_share: true,

              toolbar_menu_view: true,
              toolbar_menu_view_fit: true,
              toolbar_menu_view_fit_scroll: true,
              toolbar_menu_view_fit_width: true,
              toolbar_menu_view_fit_height: true,
              toolbar_menu_view_fit_contain: true,

              toolbar_menu_view_show: true,
              toolbar_menu_view_show_headings: true,
              toolbar_menu_view_show_row_selection: true,
              toolbar_menu_view_show_row_highlight: true,
              toolbar_menu_view_show_gridines: true,
              toolbar_menu_view_show_cell_bar: true,
              toolbar_menu_view_show_filters: true,
              toolbar_menu_view_freeze: true,

              toolbar_menu_data: true,

              toolbar_menu_page: true,

              page_views: true,

              grid_menu_copy: true,
              grid_menu_filter: true,
              grid_menu_sort: true,
              grid_menu_freeze: true,
              grid_menu_rowscols: true,
              grid_menu_cell: true,
              grid_menu_share: true,

              workspaces_tab_linking: true,
              admin_sub_organizations: true,
              admin_tags: true,
              admin_routing_rules: true,
              workspaces_show_tabbed_tile_history: true,

            },
            logo: {
              light: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/logo.svg",
              dark: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/logo.svg",
            },
          },
        },
        data_sources: [
          {
            id: 2,
            name: "Excel Add-in",
            key: "excel",
            logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/excel.png",
            appType: ['lsd'],
            help: {
              lsd: {
                desk: "https://support.ipushpull.com/data-sources/excel-add-in-source/ipushpull-documentation-excel-a-simple-live-push",
                iframe: "https://support.ipushpull.com/data-sources/excel-add-in-source/ipushpull-documentation-excel-a-simple-live-push",
              },
              dod: {
                desk: "",
                iframe: "",
              }
            }
          },
          {
            id: -1,
            name: "Database",
            key: "db",
            logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/db.svg",
            appType: ['lsd'],
            help: {
              lsd: {
                desk: "https://support.ipushpull.com/data-sources/data-loader/ipushpull-documentation-setting-up-a-push-from-your-database-to-ipushpull",
                iframe: "https://support.ipushpull.com/data-sources/data-loader/ipushpull-documentation-setting-up-a-push-from-your-database-to-ipushpull",
              },
              dod: {
                desk: "",
                iframe: "",
              }
            }
          },
        ],
          client_apps: [
            {
              id: 1,
              name: "Trayport Web Application",
              key: "trayport",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/static/Trayport/trayport_icon.png",
              help: {
                lsd: {
                  desk: "https://support.ipushpull.com/client-integrations/ipushpull-web-application/ipushpull-documentation-pull-live-and-streaming-data-into-the-ipushpull-client-app",
                  iframe: "https://support.ipushpull.com/client-integrations/ipushpull-web-application/ipushpull-documentation-pull-live-and-streaming-data-into-the-ipushpull-client-app",
                },
                dod: {
                  desk: "",
                  iframe: ""
                }
              },
              appType: ['lsd']
            },
            {
              id: 3,
              name: "Excel Add-in",
              key: "excel",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/excel.png",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://support.ipushpull.com/client-integrations/excel-add-in-client/ipushpull-documentation-excel-a-simple-live-pull",
                  iframe: "https://support.ipushpull.com/client-integrations/excel-add-in-client/ipushpull-documentation-excel-a-simple-live-pull",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            },
            {
              id: 4,
              name: "JDA",
              key: "jda",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/static/Trayport/trayport_icon.png",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "",
                  iframe: "",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            },
            {
              id: 5,
              name: "Trayport Excel Add-in",
              key: "t_excel",
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/static/Trayport/trayport_icon.png",
              appType: ['dod'],
              help: {
                lsd: {
                  desk: "",
                  iframe: "",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            },
            {
              id: 6,
              name: "Trayport API",
              key: "api",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/static/Trayport/trayport_icon.png",
              appType: ['dod'],
              help: {
                lsd: {
                  desk: "",
                  iframe: "",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            },
            {
              id: 7,
              name: "ipushpull Usage App",
              key: "ipp",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "",
                  iframe: "",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            }
        ],          
      },
    },
  },
  sso_only: false,
  sso_url: "",
  on_prem: true,
  billing: false,
  help_docs: {
    notifications_troubleshooting: {
      desk: "https://support.ipushpull.com/data-services/ipushpull-documentation-data-driven-notifications/ipushpull-documentation-troubleshooting-notifications-problems",
      iframe: "https://support.ipushpull.com/data-services/ipushpull-documentation-data-driven-notifications/ipushpull-documentation-troubleshooting-notifications-problems",
    },
    notifications_builder_excel_data_source: {
      desk: "https://support.ipushpull.com/data-sources/excel-add-in-source/ipushpull-documentation-getting-started-how-to-send-data-notifications-from-excel",
      iframe: "https://support.ipushpull.com/data-sources/excel-add-in-source/ipushpull-documentation-getting-started-how-to-send-data-notifications-from-excel",
    },
    notifications_builder_msteams_webhook: {
      desk: "https://support.ipushpull.com/client-integrations/microsoft-teams/ipushpull-documentation-create-a-microsoft-teams-incoming-webhook",
      iframe: "https://support.ipushpull.com/client-integrations/microsoft-teams/ipushpull-documentation-create-a-microsoft-teams-incoming-webhook",
    },
    notifications_builder_slack_webhook: {
      desk: "https://support.ipushpull.com/client-integrations/slack/ipushpull-documentation-create-a-slack-incoming-webhook",
      iframe: "https://support.ipushpull.com/client-integrations/slack/ipushpull-documentation-create-a-slack-incoming-webhook",
    },
  },
};
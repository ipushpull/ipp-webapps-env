const ENV = {
  ipushpull: {
    api_url: 'https://uat-trayport.ipushpull.com/api',
    api_version: '2.0',
    ws_url: 'https://uat-trayport.ipushpull.com',
    web_url: 'https://uat-trayport.ipushpull.com',
    docs_url: 'https://uat-trayport.ipushpull.com/docs',
    app_url: 'https://uat-trayport.ipushpull.com/pages',
    auth_url: 'https://uat-trayport.ipushpull.com/auth',
    billing_url: 'https://uat-trayport.ipushpull.com/billing',
    embed_url: 'https://uat-trayport.ipushpull.com/embeddedapp',
    chat_url: 'https://uat-trayport.ipushpull.com/embeddedapp/integrations/symphony/chat.html',
    api_key: 'noUCQmtIigVxj4LhXjkh75FCKV5rgd94KpudPjYr',
    api_secret:
      'Hhhrfa17sHIbsSHXeo96k7e7Yfa6GtuiKiUPjo0nOmcc2zLd7n0nKMyFLkiuNeIY7Y3FnVV5zAepCAPgLcrsZYstFDF1XkwgMLkudSWhBL8R5zOMGQVqW1kFn9evo0Nn',
    hsts: false,
    storage_prefix: 'trayport-uat',
    transport: 'polling'
  },
  clients: {
    ipushpull: {
      app: {
        embed: {
          ipushpull: {
            client_version: 'embed-test'
          },
          plugins: true
        },
        client: {
          ipushpull: {
            client_version: 'client-test'
          },
          plugins: true
        },
        auth: {
          ipushpull: {},
          images: []
        },
        symphony: {
         ipushpull: {
          storage_prefix: 'uat-trayport.symphony',
          client_version: 'symphony-test',
          api_key: 'rVLqyi0kum8MUCreuJCJxgAqYvsr6yDC48MLOrRi',
          api_secret: 'bPmflzHetMv92G9vvie0UCBqXHokIEGTplvfa0VUePxuKRPASFxJrXV1QL54ft8DmI0gXoKMYoYRapLE0uNPHmARnbnfYh5EFLLIQNtdRxt8lYxZCQkS4b3Bx4OHj4Zg'
         },
         client_application: 'symphony-embedded-app',
         destinations: true,
         plugins: true,
         formula_bar: true,
         disable_email_destinations: true,
         message_icon: 'https://ipushpull.s3.amazonaws.com/static/prd/icon-32.png',
         share_icon: 'https://ipushpull.s3.amazonaws.com/static/prd/ipushpull-thumb.png',
         integration: {
          id: 'ipp-uat-trayport.app',
          name: 'ipushpull - Trayport UAT',
          title: 'ipushpull - Trayport UAT',
          urls: {
            app: 'https://uat-trayport.ipushpull.com/syapp/',
            module: 'https://uat-trayport.ipushpull.com/embeddedapp/integrations/symphony/app.html',
            blank: 'https://uat-trayport.ipushpull.com/embeddedapp/integrations/symphony/blank.html'
          },
          icons: {
            ui: 'https://ipushpull.s3.amazonaws.com/static/prd/icon-32x16.png',
            nav: 'https://ipushpull.s3.amazonaws.com/static/prd/icon-32.png'
          },
          test: false 
         }
       },
      },
    },
   },
  app: {
    embed: {
      ipushpull: {
        client_version: 'embed-test'
      },
      plugins: true
    },
    client: {
      ipushpull: {
        client_version: 'client-test'
      },
      plugins: true
    },
    auth: {
      ipushpull: {},
      images: ['/auth/backgrounds/01.jpg', '/auth/backgrounds/02.jpg', '/auth/backgrounds/03.jpg']
    },
    symphony: {
      ipushpull: {
        client_version: 'symphony-test'
      },
      destinations: true,
      plugins: true,
      disable_email_destinations: true,
      message_icon: 'https://ipushpull.s3.amazonaws.com/static/prd/icon-32.png',
      share_icon: 'https://ipushpull.s3.amazonaws.com/static/prd/ipushpull-thumb.png',
      integration: {
        id: 'ipushpull',
        name: 'ipushpull - Trayport UAT',
        title: 'ipushpull - Trayport UAT',
        urls: {
          app: 'https://uat-trayport.ipushpull.com/syapp/',
          module: 'https://uat-trayport.ipushpull.com/embeddedapp/integrations/symphony/app.html',
          blank: 'https://uat-trayport.ipushpull.com/embeddedapp/integrations/symphony/blank.html'
        },
        icons: {
          ui: 'https://ipushpull.s3.amazonaws.com/static/prd/icon-32x16.png',
          nav: 'https://ipushpull.s3.amazonaws.com/static/prd/icon-32.png'
        }
      }
    }    
  },
  onPrem: true,
  billing: false,
};
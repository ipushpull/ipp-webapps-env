const ENV = {
  ipushpull: {
    api_url: 'https://beta.ipushpull.com/api',
    app_url: 'https://beta.ipushpull.com/pages',
    auth_url: 'https://beta.ipushpull.com/auth',
    billing_url: "https://beta.ipushpull.com/billing",
    chat_url: "https://beta.ipushpull.com/integrations/symphony/chat.html",
    embed_url: "https://beta.ipushpull.com/embedapp",
    help_url: "https://ipushpull.com/contact-support",
    api_key: "rUpB3Ls0MhbptJrBnaiU7J1LtyJHZN2qDul8qyAr",
    api_secret:
      "Vle7i5hbFPvmFFESd6j7LL48Suq0qh2VWOTeRkNbmvNVKt3vgiYYM4tEnBGlFmJjdaBUfgTsyGLoCbyvC1GmDN6o1VbUflLm6zjTaBj5hju953q8oDklVnGTfhtLuQWc",
    hsts: true,
    storage_prefix: "ipp-beta",
    transport: "polling",
  },
  clients: {
    ipushpull: {
      app: {
        auth: {
          ipushpull: {
            client_version: "auth-beta",
          },
          images: ["/auth/backgrounds/01.jpg", "/auth/backgrounds/02.jpg"],
        },
        embed: {
          ipushpull: {
            client_version: "embed-test",
          },
          plugins: true,
        },
        client: {
          demo: {
            text: "<h3>Welcome to ipushpull</h3><p>A no-code data-driven workflow platform, delivering your data wherever and whenever it's needed.</p>",
            demo_url: "https://ipushpull.com/123494576",
            demo_btn_text: "Test drive our Interactive Demo",
            video_url: "https://player.vimeo.com/video/513830429?autoplay=1",
            video_btn_text: "Watch our Platform Overview Video"
          },
          ipushpull: {
            client_version: "client-beta",
          },
          lsd: true,
          dod: true,
          stripe: {
            disable: false,
            key: "pk_live_e0JF1vfg8X6YiIdsdB6vXCzW00SZCHJkQ4",
          },
          caw: true,
          caw_popup_url: "https://ipushpull.atlassian.net/l/c/yxqDuaJH",
          theme: {
            upgrade: {
              version: "2022.9.5",
              type: "major",
              release: {
                label: "Contact Support to find out more",
                help_doc: "https://ipushpull.com/contact-support",
              },
              message:
                "<p>There's a new version of the ipushpull app available, which includes:</p><ul><li>Improved usage Dashboard</li><li>Split your Data Source usage from your Client Usage</li><li>Use Sub Organisations to group your client usage</li></ul><p>We have also updated our T&Cs which may be found <a href=http://ipushpull.com/terms/ target=\"_blank\" rel=\"noopener noreferrer\">here</a></p>",
              logo: {
                light:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/static/logo-new.svg",
                dark: "https://ipushpull.s3.eu-west-1.amazonaws.com/static/logo-white.svg",
              },
            },
            ui: {
              workspaces_tab_linking: true,
              admin_sub_organizations: true,
              admin_tags: true,
              admin_routing_rules: true,
              workspaces_show_tabbed_tile_history: true,
           },
          },
          services: [
            {
              name: "Desktop",
              key: "ipp",
              serviceType: ["lsd"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
            },
            {
              name: "Mobile",
              key: "mobile",
              serviceType: ["lsd"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
            },
            {
              name: "Excel Add-in",
              key: "excel",
              serviceType: ["lsd"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/excel.png",
            },
            {
              name: "Microsoft Teams",
              key: "teams",
              botKey: "teamsBot",
              serviceType: ["lsd", "dod", "ddn"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/teams.png",
            },
            {
              name: "Symphony",
              key: "symphony",
              botKey: "symphonyBot",
              serviceType: ["lsd", "dod", "ddn"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/symphony.png",
            },
            {
              name: "Slack",
              key: "slack",
              serviceType: ["dod", "ddn"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/slack.png",
            },
            {
              name: "WhatsApp",
              key: "whatsapp",
              serviceType: ["ddn"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/whatsapp.png",
            },
            {
              name: "SMS",
              key: "sms",
              serviceType: ["ddn"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/sms.png",
            },
            {
              name: "Email",
              key: "email",
              serviceType: ["ddn"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/email.png",
            },
            {
              name: "TaDaBeta",
              key: "tadabeta",
              serviceType: ["lsd", "dod"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/ETD_Symphonylogo.png",
            },
          ],
          data_sources: [
            {
              id: 4,
              name: "Excel Add-in",
              key: "excel",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/excel.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/AYCcJg",
                  iframe: "https://test.ipushpull.com/help.php?id=647790593",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            },
            {
              id: -1,
              name: "Database",
              key: "db",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/db.svg",
              // help_doc: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/AYDfC",
                  iframe: "https://test.ipushpull.com/help.php?id=148865025",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            },
            {
              id: 19,
              name: "TaDaBeta",
              key: "symphony",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/ETD_Symphonylogo.png",
              help_doc: "https://ipushpull.gitbook.io/euromoney-tradedata-tada-guide/getting-started",
              appType: ['lsd','dod'],
              help: {
                lsd: {
                  desk: "https://ipushpull.gitbook.io/euromoney-tradedata-tada-guide/getting-started",
                  iframe: "https://ipushpull.gitbook.io/euromoney-tradedata-tada-guide/getting-started",
                },
                dod: {
                  desk: "https://ipushpull.gitbook.io/euromoney-tradedata-tada-guide/getting-started",
                  iframe: "https://ipushpull.gitbook.io/euromoney-tradedata-tada-guide/getting-started",
                }
              }
            },
            {
              id: -1,
              name: "API",
              key: "api",
              logo: "",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/l/c/xsgmc85x",
                  iframe: "https://test.ipushpull.com/help.php?id=681607177",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            }
          ],
          client_apps: [
            {
              id: 2,
              name: "Desktop App",
              key: "ipp",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
                  iframe: "https://test.ipushpull.com/help.php?id=681705505",
                },
                dod: {
                  desk: "",
                  iframe: ""
                }
              },
              appType: ['lsd']
            },
            {
              id: 41,
              name: "Mobile App",
              key: "mobile",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/l/c/qxv9HZC1",
                  iframe: "https://test.ipushpull.com/help.php?id=650248196",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            },
            {
              id: 3,
              name: "Excel Add-in",
              key: "excel",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/excel.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
                  iframe: "https://test.ipushpull.com/help.php?id=156074055",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            },
            {
              id: 38,
              name: "Microsoft Teams App",
              key: "teams",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/teams.svg",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
                  iframe: "https://test.ipushpull.com/help.php?id=678494209",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/aut0u3UB",
                  iframe: "https://test.ipushpull.com/help.php?id=741670913",
                }
              }
            },
            {
              id: 42,
              name: "Microsoft Teams Bot",
              key: "teamsBot",
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/teams.svg",
              appType: ['dod'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
                  iframe: "https://test.ipushpull.com/help.php?id=678494209",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/aut0u3UB",
                  iframe: "https://test.ipushpull.com/help.php?id=741670913",
                }
              }
            },
            {
              id: 7,
              name: "Slack Bot",
              key: "slack",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/slack.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
              appType: ['dod'],
              help: {
                lsd: {
                  desk: "",
                  iframe: "",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/NcSUB3rn",
                  iframe: "https://test.ipushpull.com/help.php?id=1442807809",
                }
              }
            },
            {
              id: 14,
              name: "Symphony App",
              key: "symphony",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/symphony.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
                  iframe: "https://test.ipushpull.com/help.php?id=695959587",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/DeKPs5es",
                  iframe: "https://test.ipushpull.com/help.php?id=742162433",
                }
              }
            },
            {
              id: 43,
              name: "Symphony Bot",
              key: "symphonyBot",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/symphony.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
              appType: ['dod'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
                  iframe: "https://test.ipushpull.com/help.php?id=695959587",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/DeKPs5es",
                  iframe: "https://test.ipushpull.com/help.php?id=742162433",
                }
              }
            },
            {
              id: -1,
              name: "Client API",
              key: "api",
              logo: "",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/l/c/xsgmc85x",
                  iframe: "https://test.ipushpull.com/help.php?id=681607177",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            }
          ],
        },
        embed: {
          ipushpull: {
            client_version: "embed-beta",
          },
        },
        mobile: {
          ipushpull: {
            storage_prefix: "ipp-beta-mobile",
            client_version: "mobile-beta",
            api_key: "MW93WkR3oLxN701nMeX09FYCbNarDZs3CXQOJbyZ",
            api_secret:
              "rrKIW45og0608cj3aCMFuHiruTWpt1kx5jYuVinZCOWGVXeaBR6aBAMosGHLTnFRYJDIbwLuymEUKTNp7aXvRlrLvhR9YKNk9kJPomjZ8yPpLVtNFmhX5ZmrNUa2uTwJ",
          },
          client_application: "mobile-test",
        },
        symphony: {
          ipushpull: {
            storage_prefix: "ipp-beta-symphony",
            client_version: "symphony-test",
            api_key: "qK4qCYfKcIdhyYSLV829pZIVsRttZch4cCpj80Ve",
            api_secret:
              "WDpA3tyx8wQdrndx0QyUrdXjZhtzxPWRylprEt1IDNaquc6WmkfCsHVk2mufcAfkBFPFlxMnqWw6znlel5RkW9oCGY40PIjtt8InMz3sPvFEGrh3p9vYBTCxu9vJN5n2",
          },
          client_application: "beta-symphony-app",
          destinations: true,
          symphony_streams: true,
          plugins: true,
          formula_bar: true,
          disable_email_destinations: true,
          message_icon:
            "https://ipushpull.s3.amazonaws.com/static/prd/icon-32.png",
          share_icon:
            "https://ipushpull.s3.amazonaws.com/static/prd/ipushpull-thumb.png",
          integration: {
            id: "ipushpull-beta",
            name: "ipushpull - Beta",
            title: "ipushpull - Beta",
            urls: {
              app: "https://beta.ipushpull.com/syapp/",
              module:
                "https://beta.ipushpull.com/integrations/symphony/app.html",
              blank:
                "https://beta.ipushpull.com/integrations/symphony/blank.html",
            },
            icons: {
              ui: "https://ipushpull.s3.amazonaws.com/static/prd/icon-32x16.png",
              nav: "https://ipushpull.s3.amazonaws.com/static/prd/icon-32.png",
            },
            test: true,
          },
          services: [
            {
              title: "Live & Streaming Data",
              description:
                "Deliver real-time data into Symphony and other desktop and mobile apps for your team or clients",
              class: "border-color-ipp-secondary",
              color: "ipp-secondary",
              icon: "table-large",
              more_info: "https://ipushpull.com/service-live-streaming",
            },
            {
              title: "Data on Demand",
              description:
                "Pull data on demand from live systems into Symphony, or send updates with ipushpull's configurable bot service",
              class: "border-color-ipp-tertiary",
              color: "ipp-tertiary",
              icon: "robot",
              more_info: "https://ipushpull.com/service-data-on-demand",
            },
            {
              title: "Data-driven Notifications",
              description:
                "Automated, live data-driven alerts sent to your teams or clients on desktop and mobile apps",
              class: "border-color-ipp-primary",
              color: "ipp-primary",
              icon: "bell",
              more_info: "https://ipushpull.com/service-notifications",
            },
            {
              title: "Custom Applications & Workflows",
              description:
                "Customised solutions and workflows, integrated and white-labelled to order",
              class: "border-color-ipp-yellow",
              color: "ipp-yellow",
              icon: "flow-chart",
              more_info: "https://ipushpull.com/service-custom-applications",
            },
          ],
        },
        finsemble: {
          ipushpull: {
            storage_prefix: "ipp_finsemble",
          },
          plugins: true,
        },
        teams: {
          plugins: true,
          client_application: "beta-teams-app",
          more_info_url: "https://support.ipushpull.com",
          ipushpull: {
            help_url: "https://support.ipushpull.com/teams",
            storage_prefix: "ipp_teams",
            api_key: "kH7nPqFNamP8kHkBKPjNf6ezphwGHIE9fXh0owlP",
            api_secret:
              "R6wzWD9zOsIFtyMI2RWOU4pNkr8LMGns3neLAC46yythw57mCE84Pb6uivOh8d38HDHjlhGOrqSecu3WlDwhlQIPO9JgUe08mgYfqHGfo07Fw3DoBoJexStx08Qw3qOM",
          },
          guides: [
            {
              version: "2021.5.7",
              name: "finder",
              content: "Click here to choose a live data page",
              position: "bottom",
            },
            {
              version: "2021.5.7",
              name: "toolbar",
              content: "Filter and sort your data and change settings here",
              position: "bottom",
            },
            {
              version: "2021.5.7",
              name: "footer",
              content: "For help and support choose an option",
              position: "top",
            },
          ],
          integration: {
            urls: {
              app: "https://beta.ipushpull.com/roapp/",
              module: "https://beta.ipushpull.com/integrations/teams/app.html",
              remove:
                "https://beta.ipushpull.com/integrations/teams/tabremove.html",
              logout:
                "https://beta.ipushpull.com/integrations/teams/tabpersonal.html",
            },
          },
        },
      },
    },
    euromoney: {
      app: {
        symphony: {
          hide_route: true,
          __hide_page_border: true,
          plugins: true,
          client_application: "euromoney-syapp",
          ipushpull: {
            help_url: "",
            storage_prefix: "euromoney_prod_symphony",
            client_version: "symphony-prod",
            api_key: "x9NSN0oV9zNyHO3ZuEJlV1n7mN51RpMahfrzfyhJ",
            api_secret:
              "jpVvTCpaxQmbQ5uCLEbJT10J3pbP4OlGM19nTDR5t1z79WEejcvRBRXSCzQmGxio4WlKnzew80KYhSgmfkSS5kbSnyS1k4TX89mUYp9hpPwXKkiF5P5sV2nivjDQBL1e",
          },
          destinations: true,
          disable_email_destinations: true,
          message_icon:
            "https://ipushpull.s3.amazonaws.com/static/prd/icon-32.png",
          share_icon:
            "https://ipushpull.s3.amazonaws.com/static/prd/ipushpull-thumb.png",
          integration: {
            id: "euromoney-prod",
            title: "Euromoney TRADEDATA",
            name: "Euromoney TRADEDATA",
            urls: {
              app: "https://www.ipushpull.com/roapp/",
              module:
                "https://www.ipushpull.com/embeddedapp/integrations/symphony/app.html",
              blank:
                "https://www.ipushpull.com/embeddedapp/integrations/symphony/blank.html",
            },
            icons: {
              ui: "https://ipushpull.s3.amazonaws.com/static/prd/EMTD_icon_32x16.png",
              nav: "https://ipushpull.s3.amazonaws.com/static/prd/EMTD_icon_32x32.png",
            },
          },
          theme: {
            color: {
              primary: "#361744",
              secondary: "#8a70a0",
            },
            header: {
              icon: {
                light:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-square-light.svg",
                dark: "https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-square-dark.svg",
              },
            },
            home: {
              logo: {
                light:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-light.svg",
                dark: "https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-dark.svg",
              },
            },
          },
          tos_url: "https://www.euromoneytradedata.com/terms-and-conditions",
          privacy_url: "https://www.euromoneytradedata.com/privacy-policy",
          more_info_url: "https://www.euromoneytradedata.com/",
        },
        teams: {
          ipushpull: {
            storage_prefix: "euromoney_prod_teams",
            client_version: "teams-prod",
            help_url: "",
            api_key: "Qt9722tFkP4CavE4gZXH19pSiXyeSMQcR5p29dqC",
            api_secret:
              "Ojahg4E6rxh9cGOuTl8lAddP1ost0Rkj4iMPfyJvWQeLBZrinbG3OBECMc0RgBjCsoAZe2fvNUC4TlnjWqBYf3jJDvgk9obDMikmSvxt9iDpmA8i3QPxsPVXmz08B5fq",
          },
          integration: {
            urls: {
              app: "https://www.ipushpull.com/roapp/",
              module:
                "https://www.ipushpull.com/embeddedapp/integrations/teams/app.html?client=euromoney",
              remove:
                "https://www.ipushpull.com/embeddedapp/integrations/teams/tabremove.html",
            },
          },
          theme: {
            title: "Euromoney TRADEDATA",
            color: {
              primary: "#361744",
              secondary: "#8a70a0",
            },
            header: {
              icon: {
                light:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-square-light.svg",
                dark: "https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-square-dark.svg",
              },
            },
            home: {
              logo: {
                light:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-light.svg",
                dark: "https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-dark.svg",
              },
            },
          },
          client_application: "euromoney-teams-app",
          tos_url: "https://www.euromoneytradedata.com/terms-and-conditions",
          privacy_url: "https://www.euromoneytradedata.com/privacy-policy",
          more_info_url: "https://www.euromoneytradedata.com/",
        },
      },
    },
    sg_pilot: {
      app: {
        symphony: {
          hide_route: true,
          __hide_page_border: true,
          plugins: true,
          client_application: "sg-pilot-syapp",
          ipushpull: {
            help_url: "",
            storage_prefix: "sg_pilot_symphony",
            client_version: "symphony-prod",
            api_key: "XJLcZL4a7Akh9iGdl7N17dkzrT1rQL4x5HNlm94k",
            api_secret: "XHiNhFQEnBGx6jixuiVqAsr0UteJMLJNySu0NIIkulXtgkhSTfJPR51N4mk27wExN4XSTch3TdOWHg6AgJZRHwsNS47VNd6Nm6IvY5zC7mdTUjzRe4eUJz67LOngJg49",
          },
          destinations: true,
          disable_email_destinations: true,
          message_icon:
            "https://ipushpull.s3.amazonaws.com/static/prd/icon-32.png",
          share_icon:
            "https://ipushpull.s3.amazonaws.com/static/prd/ipushpull-thumb.png",
          integration: {
            id: "sg-pilot-syapp",
            title: "SG Pilot",
            name: "SG Pilot",
            urls: {
              app: "https://beta.ipushpull.com/syapp/",
              module:
                "https://beta.ipushpull.com/integrations/symphony/app.html",
              blank:
                "https://beta.ipushpull.com/integrations/symphony/blank.html",
            },
            icons: {
              ui: "https://ipushpull.s3.amazonaws.com/static/pilot/socgen_icon_32x16.png",
              nav: "https://ipushpull.s3.amazonaws.com/static/pilot/socgen_icon_32x32.png",
            },
          },
          theme: {
            header: {
              icon: {
                light:"https://ipushpull.s3.amazonaws.com/static/pilot/socgen_icon.svg",
                dark: "https://ipushpull.s3.amazonaws.com/static/pilot/socgen_icon.svg",
              },
            },
            home: {
              logo: {
                light:"https://ipushpull.s3.amazonaws.com/static/pilot/socgen_icon.svg",
                dark: "https://ipushpull.s3.amazonaws.com/static/pilot/socgen_icon.svg",
              },
            },
          },
        },
      },
    },
  },
  max_upload_size: 200,
  billing: false,
  eu_vat_countries: [
    "AT",
    "BE",
    "BG",
    "HR",
    "CY",
    "CZ",
    "DK",
    "EE",
    "FI",
    "FR",
    "DE",
    "EL",
    "HU",
    "IE",
    "IT",
    "LV",
    "LT",
    "LU",
    "MT",
    "NL",
    "PL",
    "PT",
    "RO",
    "SK",
    "SI",
    "ES",
    "SE",
  ],
  help_docs: {
    livestreaming_data_source_excel: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AYCcJg",
      iframe: "https://www.ipushpull.com/help.php?id=647790593",
    },
    livestreaming_data_source_db: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AYDfC",
      iframe: "https://www.ipushpull.com/help.php?id=148865025",
    },
    livestreaming_data_source_api: {
      desk: "https://ipushpull.atlassian.net/wiki/x/CYCgK",
      iframe: "https://www.ipushpull.com/help.php?id=681607177",
    },
    livestreaming_client_app_ipp: {
      desk: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
      iframe: "https://www.ipushpull.com/help.php?id=681705505",
    },
    livestreaming_client_app_excel: {
      desk: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
      iframe: "https://www.ipushpull.com/help.php?id=156074055",
    },
    livestreaming_client_app_symphony: {
      desk: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
      iframe: "https://www.ipushpull.com/help.php?id=695959587",
    },
    livestreaming_client_app_wp: {
      desk: "https://ipushpull.atlassian.net/wiki/x/a4CBKQ",
      iframe: "https://www.ipushpull.com/help.php?id=696352875",
    },
    livestreaming_client_app_teams: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
      iframe: "https://www.ipushpull.com/help.php?id=678494209",
    },
    notifications_builder_excel_data_source: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AYDmI",
      iframe: "https://www.ipushpull.com/help.php?id=502628353",
    },
    notifications_builder_msteams_webhook: {
      desk: "https://ipushpull.atlassian.net/wiki/x/H4BeHg",
      iframe: "https://www.ipushpull.com/help.php?id=509509663",
    },
    notifications_builder_slack_webhook: {
      desk: "https://ipushpull.atlassian.net/wiki/x/JgBfHg",
      iframe: "https://www.ipushpull.com/help.php?id=509542438",
    },
    notifications_builder_symphony_webhook: {
      desk: "https://ipushpull.atlassian.net/wiki/x/LwBfHg",
      iframe: "https://www.ipushpull.com/help.php?id=509542447",
    },
    symphony_overview: {
      desk: "https://ipushpull.atlassian.net/wiki/x/UYCHDQ",
      iframe: "https://www.ipushpull.com/help.php?id=226984017",
    },
    user_authoriziations: {
      desk: "https://ipushpull.atlassian.net/l/c/GApuWLAA",
      iframe: "https://test.ipushpull.com/help.php?id=631701505",
    },
    page_views: {
      desk: "https://ipushpull.atlassian.net/l/c/sxEHKpxC",
      iframe: "https://test.ipushpull.com/help.php?id=1431175169",
    },
    workspaces: {
      desk: "https://ipushpull.atlassian.net/l/c/3BpmrK5E",
      iframe: "https://test.ipushpull.com/help.php?id=171442177",
    },
  },
};